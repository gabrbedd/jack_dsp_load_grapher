#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <string.h>

#include <jack/jack.h>
#include <jack/ringbuffer.h>
#include <jack/transport.h>

struct dsp_load_grapher_data_t {
        jack_client_t *client;
        jack_ringbuffer_t *rb;
        jack_nframes_t sample_rate;
};

struct dsp_load_msg_t {
        jack_nframes_t frame;
        jack_nframes_t transport_frame;
        float cpu_load;
};

struct dsp_load_grapher_data_t g_data;
volatile int g_running = 1;

static int
process_callback(jack_nframes_t nframes, void* arg)
{
        struct dsp_load_grapher_data_t *data;
        struct dsp_load_msg_t msg;
        jack_position_t pos;
        size_t w;

        if(! g_running )
                return 0;

        data = (struct dsp_load_grapher_data_t*)arg;

        jack_transport_query(data->client, &pos);

        msg.frame = jack_last_frame_time(data->client);
        msg.transport_frame = pos.frame;
        msg.cpu_load = jack_cpu_load(data->client);

        w = jack_ringbuffer_write_space(data->rb);
        if( w >= sizeof(msg) ) {
                jack_ringbuffer_write(data->rb, (void*)&msg, sizeof(msg));
        }

        return 0;
}

static void
signal_handler(int sig)
{
        if(sig == SIGINT) {
                g_running = 0;
        }
}

// Size the ringbuffer for about 5 seconds of callbacks
// when sample rate is 48000 and 64 frames/period...
// rounded up to nearest power of 2.
#define NMESSAGES (1L<<12)

int
main(int argc, char* argv[])
{
        struct dsp_load_msg_t buf[NMESSAGES], *it;
        const char* filename;
        FILE *fp;
        size_t r;
        struct sigaction sig_hand;

        if(argc != 2) {
                assert(argc > 0);
                printf("Usage: %s [filename]\n", argv[0]);
                exit(0);
        }

        filename = argv[1];
        g_data.client = jack_client_open("jack_dsp_load_grapher",
                                         JackNullOption,
                                         0 );
        if( ! g_data.client ) {
                printf("Error: could not connect to jack\n");
                exit(1);
        }
        if( jack_set_process_callback(g_data.client, process_callback, (void*)&g_data) ){
                printf("Error: could not set client callback\n");
                exit(1);
        }
                    
        g_data.rb = jack_ringbuffer_create( NMESSAGES * sizeof(struct dsp_load_msg_t) );
        if( ! g_data.rb ) {
                printf("Error: could not allocate a ringbuffer\n");
                exit(1);
        }

        g_data.sample_rate = jack_get_sample_rate(g_data.client);

        fp = fopen(filename, "wt");
        if( ! fp ) {
                printf("Error: could not open file `%s`\n", filename);
                exit(1);
        }

        memset(&sig_hand, 0, sizeof(sig_hand));
        sig_hand.sa_handler = signal_handler;

        if( sigaction(SIGINT, &sig_hand, 0) ) {
                printf("Error: could not set up signal handler\n");
                exit(1);
        }

        printf("Press ^C to exit program.\n");

        if( jack_activate(g_data.client) ) {
                printf("Error: could not activate jack client\n");
                exit(1);
        }

        fprintf(fp, "#elapsed time (sec)\telapsed transport time(sec)\tcpu load (%%)\n");
        while(g_running) {
                r = jack_ringbuffer_read_space(g_data.rb);
                if(r) {
                        double secs;
                        double trans_secs;
                        if(r > NMESSAGES * sizeof(struct dsp_load_msg_t)) {
                                r = NMESSAGES * sizeof(struct dsp_load_msg_t);
                        }
                        r = jack_ringbuffer_read(g_data.rb, (char*)buf, r);
                        r /= sizeof(struct dsp_load_msg_t);
                        it = buf;
                        while(r--) {
                                secs = ((double)(it->frame)) / g_data.sample_rate;
                                trans_secs = ((double)(it->transport_frame)) / g_data.sample_rate;
                                fprintf(fp, "%g\t%g\t%g\n", secs, trans_secs, (it->cpu_load));
                                ++it;
                        }
                }
                fflush(fp);
        }

        jack_deactivate(g_data.client);
        jack_client_close(g_data.client);
        fclose(fp);
        jack_ringbuffer_free(g_data.rb);

        return 0;
}
