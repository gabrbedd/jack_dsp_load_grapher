CFLAGS = -g -O3

all: jack_dsp_load_grapher

jack_dsp_load_grapher: jack_dsp_load_grapher.o
	$(CC) $(CFLAGS) -o $@ $^ -ljack
